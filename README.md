# exmaple weather app on svelte js 

This is a project exmaple project.

# Vendor
 - template css and html from codepen.
    - url : https://codepen.io/Call_in/pen/pMYGbZ

 - weather api from openweathermap.
    - url : https://api.openweathermap.org/

## Get started
Install the dependencies...

```bash
npm install
```

Start app

```bash
npm run dev
```